package davidneilan.com;

import org.newdawn.slick.Color;

public interface Clickable {
    public void onClick();
    public Color getColor();
}
